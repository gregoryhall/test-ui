import { setFiles } from '../redux/actions';

export const getFiles = (token) => {
    return async (dispatch, getState) => {

        try {
            const apiUrl = '/api/files';
            const headers = {"Accepts": "application/json"};
            if (token) {
              headers["Authorization"] = `Bearer ${token}`;
            }
            //console.log('Fetching files.. ' +  headers.Authorization);
            fetch(apiUrl, {
                headers,
                credentials: "include"
              },
              (error) => { console.warn("Error:" + error) }
              )
              .then(response => response.json())
              .then(filenames => {
                 //console.log("thunk pulled files " + filenames);
                 return dispatch(setFiles(token, filenames));
              });
        } catch (e) {
            console.log('Fetch Error: ', e)
        }
    };
};

export const moveFile = (token, filename, filebase64) => {
   return async  (dispatch, getState) => {
        try {
           var dataUrlParts = filebase64.base64.split(',');
           //var mimeType   = dataUrlParts[0];
           var base64Data = dataUrlParts[1];
//           console.log("moveFile thunk => token: "
//                       + token + " name: " + filename + " CONTENTS..." + { base64Data });
           const headers = { "Content-Type": "application/octet-stream",
                             "Content-Encoding": "base64",
                             "Transfer-Encoding": "base64" };
           if (token) {
              headers["Authorization"] = `Bearer ${token}`;
           }
           console.log("posting w/ headers..." + JSON.stringify(headers));
           await fetch(`/api/files/${filename}`, {
                  method: 'POST',
                  headers,
                  credentials: "include",
                  body: base64Data
                 },
                 (error) => { console.warn("Error:" + error) }
                 )
                .then(() => dispatch(getFiles(token)));
         }
         catch (e) {
            console.log('Fetch Error: ', e)
         }
       };
};