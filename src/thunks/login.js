import { LOGIN } from '../redux/actionTypes';
import { loginComplete } from '../redux/actions';
import { useDispatch as dispatch } from "react-redux";
import Keycloak from 'keycloak-js';

export const login = () => {
    const keycloak = Keycloak('../keycloak.json');
    //console.log("Login hit thunk...");
    const loginPromise = keycloak.init({onLoad: 'login-required',
                                        flow: 'standard',
                                        scope: 'openid profile roles' });
    // This is the Promise
    return loginPromise.then(async (authenticated) => {
            //console.log("Thunk login complete: " + JSON.stringify(keycloak));
            // This is the type the Promise holds
            return loginComplete( authenticated, keycloak, keycloak.token );
          });
};
