import React, { Component, setState, useState, useEffect } from 'react';
import { useSelector, useDispatch as dispatch, connect } from 'react-redux';
import Files from './Files';
import { login } from '../thunks/login';
import { getFiles } from '../thunks/files';
import { loginBegin, loginComplete } from '../redux/actions';

class Secured extends Component {

  componentDidMount() {
    this.props.login();
  }

  render() {
    if (this.props.authenticated) {
      return (
        <div><b>This is a Keycloak-secured component of your application.<br/>
          You shouldn't be able to see this unless you've authenticated with Keycloak.</b>
          <Files />
        </div>
      );
    }
    else
      return ( <div><b>Unable to authenticate!</b></div> );
  }
}

const mapStateToProps = (state) => {
    //console.log("Secured => Initializing props from state...");
    return { authenticated: state.login.authenticated,
             keycloak: state.login.keycloak,
             token: state.login.token };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    // dispatching login begin and completed actions
    login: () => {
      dispatch(loginBegin());
      login().then((loginComplete) => {
        dispatch(loginComplete);
        dispatch(getFiles(loginComplete.payload.token));
        return loginComplete;
      });
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Secured);
