import React, { Component } from 'react';

class Welcome extends Component {
  render() {
    return (
      <div className="Welcome">
        <p>This is a public-facing component.</p>
      </div>
    );
  }
}
export default Welcome;