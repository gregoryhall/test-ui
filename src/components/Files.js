import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { connect, useSelector, useDispatch } from 'react-redux';
import { setFilename, setFileBase64 } from '../redux/actions'
import { getFiles, moveFile } from '../thunks/files';
import FileBase64 from 'react-file-base64';
import Grid from '@material-ui/core/Grid';
import { Button, TextField } from '@material-ui/core';

const Files = (props) => {
   
    console.log(props);
    
    const dispatch = useDispatch();

    const token = useSelector(state => state.login.token);
    const filenames = useSelector(state => state.files.filenames);
    const filename = useSelector(state => state.files.filename);
    const fileBase64 = useSelector(state => state.files.fileBase64);

//    useEffect(() => {
//        console.log(`Files => On load token: ${token} files: ${filenames} data:` +
//                    JSON.stringify(fileBase64));
//    });

    const useStyles = makeStyles({
      table: {
        minWidth: 64
      },
    });
    const classes = useStyles();
    const tok = props.hasOwnProperty('token') ?
                                  props.token : token;
    return (
         <TableContainer component={Paper}>
           <Table className={classes.table} aria-label="simple table">
              <TableHead>
                 <TableRow>
                   <TableCell align="left">Filename</TableCell>
                 </TableRow>
              </TableHead>
              <TableBody>
                {
                   filenames.map(fname => {
                   return (
                    <TableRow key={fname}>
                      <TableCell align="left" component="th" scope="row">
                        {fname}
                      </TableCell>
                    </TableRow>
                   );
                   })
                 }
                </TableBody>
            </Table>
            <Grid item xs={10}>
             <FileBase64
                     multiple={ false }
                     onDone={ (fileb64) => {
                         //console.log( "files base64:" + JSON.stringify(fileb64) );
                         dispatch(setFileBase64(tok, fileb64));
                         if (! props.filename) {
                           dispatch(setFilename(tok, fileb64.name));
                         }
                       }
                     }>
                <b>Choose File</b>
             </FileBase64>
            </Grid>
            <Grid item xs={4}>
              <TextField label="Name" variant="outlined" value={filename}
                         onChange={(e) => { dispatch(setFilename(tok, e.target.value)) }}/>
              <Button variant="contained"
                      onClick={(e) => {
                        props.uploadFile(tok,
                                         props.filename ?
                                         props.filename: props.filename,
                                         props.fileBase64); }
                      }>
                <b>Upload File</b>
              </Button>
            </Grid>
    </TableContainer>
    );
};

const mapStateToProps = (state) => {
    //console.log("Files => Initializing props from state..." + { state });
    return { token: state.hasOwnProperty('login') ?
                    state.login.token :
                    state.hasOwnProperty('token') ?
                    state.token : '',
             filenames: state.files.filenames,
             filename: state.files.filename,
             fileBase64: state.files.fileBase64
           };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    uploadFile: (token, filename, fileBase64) => {
       console.log("Uploading file: " + JSON.stringify(fileBase64));
       dispatch(moveFile(token, filename, fileBase64));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Files);