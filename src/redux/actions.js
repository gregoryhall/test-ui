// probably want to decompose once enough actions are in here

import {
  LOGIN_REQUESTED,
  LOGIN_COMPLETE,
  SET_FILES,
  SET_FILENAME,
  SET_FILEBASE64
 } from "./actionTypes";

export const loginBegin = () => ({
  type: LOGIN_REQUESTED
});

export const loginComplete = (authenticated, keycloak, token) => ({
  type: LOGIN_COMPLETE,
  payload: {
    authenticated,
    keycloak,
    token
  }
});

// Files
export const setFiles = (token, filenames) => ({
  type: SET_FILES,
  payload: {
    token,
    filenames
  }
});

export const setFilename = (token, filename)  => ({
  type: SET_FILENAME,
  payload: {
    token,
    filename
  }
});

export const setFileBase64 = (token, fileBase64) => ({
  type: SET_FILEBASE64,
  payload: {
     token,
     fileBase64
  }
});
