import { combineReducers } from 'redux';

import login from './login';
import files from './files';

export default combineReducers({ login, files });
