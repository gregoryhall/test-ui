import { LOGIN_REQUESTED, LOGIN_COMPLETE } from "../actionTypes";
import { useDispatch as dispatch } from 'react-redux';
import { login } from '../../thunks/login';

const initialState = {
     keycloak: null,
     authenticated: false,
     token: ''
};

export default (state = initialState, action) => {
  const actionType = (action || {}).type;
  if (actionType == LOGIN_REQUESTED) {
    //console.log("Login started in reducer...");
    return {
      authenticated: false,
      keycloak: null,
      token: ''
    };
  }
  else if(actionType === LOGIN_COMPLETE) {
    if (action.payload.authenticated) {
      // console.log(`LOGIN_COMPLETE token is in the reducer ${action.payload.token}`);
    } else {
       console.log('Login failed');
    }
    return {
      authenticated: action.payload.authenticated,
      keycloak: action.payload.keycloak,
      //convenience
      token: action.payload.token
    };
  }
  return state;
};