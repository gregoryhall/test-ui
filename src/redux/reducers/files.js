import { SET_FILES, SET_FILENAME, SET_FILEBASE64 } from "../actionTypes";
import { getFiles } from "../../thunks/files";
import { useDispatch } from "react-redux";

const initialState = {
     token: null,
     filenames: [],
     filename: '',
     fileBase64: null
};

export default (state = initialState, action) => {
  const actionType = (action || {}).type;

  if(actionType === SET_FILES) {
    //console.log("reducer setting files..." + action.payload.filenames);
    return {
         token: action.payload.token,
         filenames: action.payload.filenames,
         filename: state.filename,
         fileBase64: state.fileBase64
     };
  }
  else if(SET_FILENAME == actionType) {
    return {
             token: action.payload.token,
             filenames: state.filenames,
             filename: action.payload.filename,
             fileBase64: state.fileBase64
         };
  }
  else if (SET_FILEBASE64 == actionType) {
    return {
       token: action.payload.token,
       filename: state.filename,
       filenames: state.filenames,
       fileBase64: action.payload.fileBase64
    };
  }

  return state;
};