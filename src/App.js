import React, { Component } from 'react';
import Welcome from './components/Welcome';
import Secured from './components/Secured';
import './App.css';

class App extends Component {

  render() {
    return (
        <div className="container">
            <img src='logo.svg' className="App-logo" alt="logo" />
            <Welcome />
            <Secured />
       </div>
    );
  }
}
export default App;
